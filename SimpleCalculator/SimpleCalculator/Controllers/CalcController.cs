﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleCalculator.Models;
namespace SimpleCalculator.Controllers
{
    public class CalcController : Controller
    {
        // GET: Calc
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(Calc c, string calculate)
        {
            if (calculate == "add")
            {
                c.answer = c.firstNumber + c.secondNumber;
            }
            else if (calculate == "sub")
            {
                c.answer = c.firstNumber - c.secondNumber;
            }
            else if (calculate == "mul")
            {
                c.answer = c.firstNumber * c.secondNumber;
            }
            else
            {
                c.answer = c.firstNumber / c.secondNumber;

            }
            return View(c);
        }
    }
}