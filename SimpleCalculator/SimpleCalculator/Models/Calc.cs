﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleCalculator.Models
{
    public class Calc
    {
        public double firstNumber { get; set; }
        public double secondNumber { get; set; }
        public double answer { get; set; }

    }
}