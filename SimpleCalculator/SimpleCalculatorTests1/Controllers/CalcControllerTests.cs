﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleCalculator.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleCalculator.Models;
using System.Web.Mvc;

namespace SimpleCalculator.Controllers.Tests
{
    [TestClass()]
    public class CalcControllerTests
    {
        [TestMethod()]
        public void IndexTest()
        {
            //Arrange
            CalcController controller = new CalcController();
            Calc c1 = new Calc();
            c1.firstNumber = 10;
            c1.secondNumber = 20;
            var expected = -10;
            string calculate = "sub";

            //Act
            var actual = controller.Index(c1, calculate) as ViewResult;
            var model = (Calc)actual.Model;

            //Assert

            Assert.AreEqual(expected, model.answer);
        }
    }
}